package examplesoureit.contactlistsourceithomework;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    public static final String MY_TAG = "object";

    //    @BindView(R.id.list_view)
//    ListView listView;
    @BindView(R.id.list_view)
    RecyclerView recyclerView;

    List<Contact> contactList = new ArrayList<>();

    //    ContactAdapter contactAdapter;
    ContactRecyclerAdapter contactRecyclerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        generateContactList();


        contactRecyclerAdapter = new ContactRecyclerAdapter(getApplicationContext(), contactList);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(contactRecyclerAdapter);


//        contactAdapter = new ContactAdapter(this, contactList);
//        Log.d("my_tag", contactList.get(0).getName());
//        listView.setAdapter(contactAdapter);
//
//        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                Contact contact = contactList.get(position);
//                Log.d("my_tag", "onClickItem pressed");
//                Intent intent = new Intent(MainActivity.this, ContactInformation.class);
//                Log.d("my_tag", "intent created");
//                Log.d("my_tag", "Contact maiinActivity is null: "+String.valueOf(contact==null));
////                intent.putExtra(MY_TAG, contact);
//
//                intent.putExtra("nameContact", contact.getName());
//                intent.putExtra("imageContact", contact.getImage());
//                intent.putExtra("emailContact", contact.getEmail());
//                intent.putExtra("phoneContact", contact.getPhone());
//                intent.putExtra("addressContact", contact.getAddress());
//
//
//                startActivity(intent);
//            }
//        });


    }

//    private EditText name;
//    private EditText email;
//    private EditText phone;
//    private EditText address;

    @OnClick(R.id.action)
    public void onPress() {

        LayoutInflater inflater = LayoutInflater.from(MainActivity.this);
        View subview = inflater.inflate(R.layout.dialog, null);
        final EditText editTextName = subview.findViewById(R.id.dialog_name);
        final EditText editTextEmail = subview.findViewById(R.id.dialog_email);
        final EditText editTextPhone = subview.findViewById(R.id.dialog_phone);
        final EditText editTextAddress = subview.findViewById(R.id.dialog_address);

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Enter name of new contact");
        builder.setView(subview);

        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                contactList.add(new Contact(editTextName.getText().toString(),
                        R.drawable.contact2,
                        editTextEmail.getText().toString(),
                        editTextPhone.getText().toString(),
                        editTextAddress.getText().toString()));
                contactRecyclerAdapter.notifyDataSetChanged();
            }
        });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Toast.makeText(MainActivity.this, "Cancel", Toast.LENGTH_SHORT).show();
            }
        });

        AlertDialog alertDialog = builder.create();

        alertDialog.show();
    }

    private void generateContactList() {

        contactList.add(new Contact("Peter", R.drawable.contact1, "notak@me.ow", "+3806845887", "Buchmu 1"));
        contactList.add(new Contact("Sergio", R.drawable.contact2, "sergio@me.ow", "+3806789887", "Petra Grigorenko"));
        contactList.add(new Contact("Oleg", R.drawable.contact3, "oleg@me.ow", "+3806841564", "Ludviga Svobodu"));

    }
}
