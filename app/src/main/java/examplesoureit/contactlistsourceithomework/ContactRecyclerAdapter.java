package examplesoureit.contactlistsourceithomework;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ContactRecyclerAdapter extends RecyclerView.Adapter<ContactRecyclerAdapter.ViewHolder> implements View.OnClickListener, View.OnLongClickListener{

    List<Contact> contactList = new ArrayList<>();
    Context context;

    public ContactRecyclerAdapter(Context context, List<Contact> contactList) {
        this.contactList = contactList;
        this.context = context;
    }

    @NonNull
    @Override
    public ContactRecyclerAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_contact, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ContactRecyclerAdapter.ViewHolder holder, final int position) {
        holder.image.setImageResource(contactList.get(position).getImage());
        holder.name.setText(contactList.get(position).getName());
        holder.email.setText(contactList.get(position).getEmail());

        holder.relativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ContactInformation.class);
                intent.putExtra("nameContact", contactList.get(position).getName());
                intent.putExtra("imageContact", contactList.get(position).getImage());
                intent.putExtra("emailContact", contactList.get(position).getEmail());
                intent.putExtra("phoneContact", contactList.get(position).getPhone());
                intent.putExtra("addressContact", contactList.get(position).getAddress());

                intent.putExtra(Contact.class.getCanonicalName(), contactList.get(position));

                v.getContext().startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return contactList.size();
    }

    @Override
    public void onClick(View v) {

    }

    @Override
    public boolean onLongClick(View v) {
        return false;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.image)
        ImageView image;

        @BindView(R.id.name)
        TextView name;

        @BindView(R.id.email)
        TextView email;

        @BindView(R.id.item_relative_layout)
        RelativeLayout relativeLayout;

        public ViewHolder(View item) {
            super(item);
            ButterKnife.bind(this, item);

        }
    }
}
