package examplesoureit.contactlistsourceithomework;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ContactInformation extends AppCompatActivity {

    @BindView(R.id.contact_name)
    TextView name;
    @BindView(R.id.image)
    ImageView image;
    @BindView(R.id.email)
    TextView email;
    @BindView(R.id.phone_number)
    TextView phone;
    @BindView(R.id.address)
    TextView address;

    Contact contact;

    @Override
    protected void onCreate(Bundle savedInstanceState) throws RuntimeException {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_information);
        ButterKnife.bind(this);
//        Log.d("my_tag", "Yo");
//        Contact contact = null;
//        try {
//            contact = getIntent().getExtras().getParcelable(MainActivity.MY_TAG);
//        } catch (RuntimeException e) {
//            Log.d("my_tag", "RunTime Exception ");
//        }
//        Log.d("my_tag", "contact delevered");
//        Log.d("my_tag", String.valueOf(contact == null));
//        Log.d("my_tag", getIntent().getExtras().getString("nameContact"));

        contact = (Contact) getIntent().getParcelableExtra(Contact.class.getCanonicalName());

        if (contact != null) {

        name.setText(contact.getName());
        image.setImageResource(contact.getImage());
        email.setText(contact.getEmail());
        phone.setText(contact.getPhone());
        address.setText(contact.getAddress());

//            name.setText(getIntent().getStringExtra("nameContact"));
//            image.setImageResource(getIntent().getIntExtra("imageContact",0));
//            email.setText(getIntent().getStringExtra("emailContact"));
//            phone.setText(getIntent().getStringExtra("phoneContact"));
//            address.setText(getIntent().getStringExtra("addressContact"));
        }
    }
}
