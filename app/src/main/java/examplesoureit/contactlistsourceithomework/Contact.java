package examplesoureit.contactlistsourceithomework;

import android.media.Image;
import android.os.Parcel;
import android.os.Parcelable;

public class Contact implements Parcelable {
    private String name;
    private int image;
    private String email;
    private String phone;
    private String address;

    public Contact(String name, int image, String email, String phone, String address) {
        this.name = name;
        this.image = image;
        this.email = email;
        this.phone = phone;
        this.address = address;
    }

//    protected Contact(Parcel in) {
//        name = in.readString();
//        image = in.readInt();
//        email = in.readString();
//        phone = in.readString();
//        address = in.readString();
//    }

//    public static final Creator<Contact> CREATOR = new Creator<Contact>() {
//        @Override
//        public Contact createFromParcel(Parcel in) {
//            return new Contact(in);
//        }
//
//        @Override
//        public Contact[] newArray(int size) {
//            return new Contact[size];
//        }
//    };

    protected Contact(Parcel in) {
        name = in.readString();
        image = in.readInt();
        email = in.readString();
        phone = in.readString();
        address = in.readString();
    }

    public static final Creator<Contact> CREATOR = new Creator<Contact>() {
        @Override
        public Contact createFromParcel(Parcel in) {
            return new Contact(in);
        }

        @Override
        public Contact[] newArray(int size) {
            return new Contact[size];
        }
    };

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeInt(image);
        dest.writeString(email);
        dest.writeString(phone);
        dest.writeString(address);
    }

//    @Override
//    public int describeContents() {
//        return 0;
//    }
//
//    @Override
//    public void writeToParcel(Parcel dest, int flags) {
//        dest.writeString(name);
//        dest.writeInt(image);
//        dest.writeString(email);
//        dest.writeString(phone);
//        dest.writeString(address);
//    }
}
