package examplesoureit.contactlistsourceithomework;

import android.content.Context;
import android.media.Image;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ContactAdapter extends BaseAdapter {

    private Context context;
    private List<Contact> contacts;

    public ContactAdapter(Context context, List<Contact> contacts) {
        this.context = context;
        this.contacts = contacts;
    }

    @Override
    public int getCount() {
        return contacts.size();
    }

    @Override
    public Contact getItem(int position) {
        return contacts.get(position);
    }

    @Override
    public long getItemId(int position) {
        return contacts.get(position).hashCode();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View rowView = convertView;
        ViewHolder holder;
        if(rowView==null){
            rowView = LayoutInflater.from(context)
                    .inflate(R.layout.item_contact, parent, false);
            holder = new ViewHolder(rowView);
            rowView.setTag(holder);
        } else{
            holder = (ViewHolder) rowView.getTag();
        }

        holder.name.setText(getItem(position).getName());
        holder.image.setImageResource(getItem(position).getImage());
        holder.name.setText(getItem(position).getEmail());

        return rowView;
    }

    static class ViewHolder{
        @BindView(R.id.name)
        TextView name;

        @BindView(R.id.image)
        ImageView image;

        @BindView(R.id.email)
        TextView email;
//
//        @BindView(R.id.phone_number)
//        TextView phone;
//
//        @BindView(R.id.address)
//        TextView address;

        public ViewHolder(View root) {
            ButterKnife.bind(this, root);
        }
    }
}
